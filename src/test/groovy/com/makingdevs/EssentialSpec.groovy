package com.makingdevs

import spock.lang.Specification

class EssentialSpec extends Specification {

  def "Simple specification example"(){
    given:
      List<String> list = []
    when:
      list << "Groovy"
    then:
      list == ["Groovy"]
  }
}
