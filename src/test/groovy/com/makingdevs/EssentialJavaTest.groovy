package com.makingdevs

import org.junit.Test
import static org.junit.Assert.*
import static org.hamcrest.core.IsCollectionContaining.*

public class EssentialJavaTest {
  @Test
  public void simpleTest(){
    // given:
    List<String> list = new ArrayList<>();
    // when:
    list.add("Groovy")
    // then:
    assertThat(list, hasItem("Groovy"))
  }

}
