package com.makingdevs

import spock.lang.*

class InvoiceServiceSpec extends Specification {

  def "the tax for product"() {
    given:
    InvoiceService service = new InvoiceServiceImpl()
    Product product = new Product(description: "Some", quantity: 3, unitPrice: 12.3)
    when:
    BigDecimal tax = service.calculateTaxForProduct(product, 0.16)
    then:
    tax == 5.904
  }

  @Unroll(""" #quantity product(s) at #unitPrice
              and a tax of #tax give us #computed_tax_expected""")
  def "the tax for some products in pipeline"() {
    given:
    InvoiceService service = new InvoiceServiceImpl()
    Product product = new Product(description: "Some", quantity: quantity, unitPrice: unitPrice)
    when:
    BigDecimal expected_tax  = service.calculateTaxForProduct(product, tax)
    then:
    expected_tax == computed_tax_expected
    where:
    quantity | unitPrice | tax || computed_tax_expected
    1.0      | 1.0       | 1.0 || 1.0
    2.0      | 3.0       | 0.9 || 5.4
  }

  def "read the product and compute the task"() {
    given:
    InvoiceService service = new InvoiceServiceImpl()
    and:
    ProductRepository repository = Stub()
    repository.get(_) >> new Product(quantity: 15.0, unitPrice: 2.0)
    service.productRepository = repository
    when:
    BigDecimal expected_tax  = service.calculateTaxForProduct(1234, 0.7)
    then:
    expected_tax ==  21.0
  }

  def "make sure of reading the product from the origin"() {
    given:
    InvoiceService service = new InvoiceServiceImpl()
    and:
    ProductRepository repository = Mock()
    service.productRepository = repository
    when:
    service.calculateTaxForProduct(1234, 0.7)
    then:
    1 * repository.get(_) >> new Product(quantity: 0.0, unitPrice: 0.0)
  }

  def """read the product and make sure of
        reading the product from the origin to compute the tax"""() {
    given:
    InvoiceService service = new InvoiceServiceImpl()
    and:
    ProductRepository repository = Mock()
    service.productRepository = repository
    when:
    def expected_tax = service.calculateTaxForProduct(1234, 0.7)
    then:
    1 * repository.get(_) >> new Product(quantity: 15.0, unitPrice: 2.0)
    expected_tax ==  21.0
  }
}
