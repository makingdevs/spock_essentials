package com.makingdevs;

public interface ProductRepository {
  Product get(Long id);
}

