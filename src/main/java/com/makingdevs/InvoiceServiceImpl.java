package com.makingdevs;

import java.math.BigDecimal;

public class InvoiceServiceImpl implements InvoiceService {

  private ProductRepository productRepository;

  public void setProductRepository(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  @Override
  public BigDecimal calculateTaxForProduct(Long productId, BigDecimal tax) {
    Product product = productRepository.get(productId);
    return calculateTaxForProduct(product, tax);
  }

  @Override
  public BigDecimal calculateTaxForProduct(Product product, BigDecimal tax) {
    BigDecimal computedTax = product.getQuantity().multiply(product.getUnitPrice()).multiply(tax);
    return computedTax;
  }
}
