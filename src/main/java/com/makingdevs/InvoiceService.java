package com.makingdevs;

import java.math.BigDecimal;

public interface InvoiceService {
  BigDecimal calculateTaxForProduct(Long productId, BigDecimal tax);
  BigDecimal calculateTaxForProduct(Product product, BigDecimal tax);
}
